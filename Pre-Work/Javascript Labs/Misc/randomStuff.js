//var hellosWanted = 12;

/*While Loop Solution
var hellosPrinted = 0;

while(hellosWanted >= hellosPrinted){
    console.log("hello! #" + hellosPrinted);
    hellosPrinted++;
}*/

/*For Loop Solution
for(hellosPrinted=0; hellosWanted >= hellosPrinted; hellosPrinted++){
    if(hellosPrinted % 2 === 0){
        console.log("hello! #" + hellosPrinted);
    } 
}*/

var stepNum = 3;
var startNum = 4;
var evensWanted = 5;
var evenNumList = [];

for(evensFound = 1; evensFound <= evensWanted;){
    startNum += stepNum;
    if(startNum % 2 == 0){
        evenNumList.push(startNum);
        evensFound++;
    }
}
console.log(evenNumList);

/*var stepNum = 3;
var startNum = 4;
var evensWanted = 5;
var evenNumList = [];

var evensFound = 0;

while(evensFound <= evensWanted){
    startNum += stepNum;
    if (startNum % 2 === 0){
        evenNumList.push(startNum);
        evensFound++;
    }
}
console.log(evenNumList);*/