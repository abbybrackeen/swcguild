
function validateItems(numToValidate) {
	if (numToValidate == "" || isNaN(numToValidate)) {
		alert(numToValidate + "is not a valid entry.");
		return false;
	}
	return true;
}


function handleSubmit(){
	var startNum = document.forms["userInput"]["startNum"].value;
	var numOfEvensWanted = document.forms["userInput"]["numOfEvens"].value;

	if (!validateItems(startNum) || !validateItems(numOfEvensWanted)) {
		return false;
	}
	
	var iStartNum = Number(startNum);
	var iNumOfEvensWanted = Number(numOfEvensWanted);

	var evenNumbers = [];
	var numOfEvensFound = 1;

	while (iNumOfEvensWanted >= numOfEvensFound){
		iStartNum++;
		if(iStartNum % 2 == 0) {
			numOfEvensFound++;
			evenNumbers.push(iStartNum);
		}
	}
	
	document.getElementById("startNumResult").innerText = iStartNum;
	document.getElementById("numOfEvensResult").innerText = iNumOfEvensWanted;
	document.getElementById("arrayResult").innerText = evenNumbers;

	document.getElementById("results").style.display = "block";
	return false;
}